import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

// servicios
import { PeliculasService } from "./services/peliculas.service";
import { HeroesService } from "./services/heroes.service";

// Pipes
import { PeliculaImagenPipe } from './pipes/pelicula-imagen.pipe';
import { KeysPipe } from './pipes/keys.pipe';

import { APP_ROUTING } from "./app.routes";

import { AppComponent } from './app.component';
import { DemoComponent } from "./components/demo/demo.component";
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { PeliculaComponent } from './components/pelicula/pelicula.component';
import { BuscarComponent } from './components/buscar/buscar.component';
import { GaleriaComponent } from './components/home/galeria.component';
import { HeroesComponent } from './components/home/heroes.component';

@NgModule({
  declarations: [
    AppComponent,
    DemoComponent,
    NavbarComponent,
    HomeComponent,
    PeliculaComponent,
    BuscarComponent,
    PeliculaImagenPipe,
    KeysPipe,
    GaleriaComponent,
    HeroesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    APP_ROUTING
  ],
  providers: [
    PeliculasService,
    HeroesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
